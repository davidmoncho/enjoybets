<!DOCTYPE html>
<html dir="ltr" lang="es-ES">
<head>

    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="author" content="David Moncho" />

    <!-- Stylesheets
    ============================================= -->
    <link href="http://fonts.googleapis.com/css?family=Lato:300,400,400italic,600,700|Raleway:300,400,500,600,700|Crete+Round:400italic" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="css/bootstrap.css" type="text/css" />
    <link rel="stylesheet" href="style.css" type="text/css" />
    <link rel="stylesheet" href="css/swiper.css" type="text/css" />
    <link rel="stylesheet" href="css/dark.css" type="text/css" />
    <link rel="stylesheet" href="css/font-icons.css" type="text/css" />
    <link rel="stylesheet" href="css/animate.css" type="text/css" />
    <link rel="stylesheet" href="css/magnific-popup.css" type="text/css" />

    <link rel="stylesheet" href="css/responsive.css" type="text/css" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <!--[if lt IE 9]>
    <script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
    <![endif]-->

    <!-- Document Title
    ============================================= -->
    <title>EnjorBets | Diviertete apostando</title>

</head>

<body class="stretched dark">

<!-- Document Wrapper
============================================= -->
<div id="wrapper" class="clearfix">

    @include('headers.header_login')
    @yield('content')
    @include('footers.footer')


</div><!-- #wrapper end -->

<!-- Go To Top
============================================= -->
<div id="gotoTop" class="icon-angle-up"></div>

<!-- External JavaScripts
============================================= -->
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/plugins.js"></script>

<!-- Footer Scripts
============================================= -->
<script type="text/javascript" src="js/functions.js"></script>

<script type="text/javascript">
    $(function() {

        var menu_ul = $('.menu_lateral > li > ul'),
                menu_a  = $('.menu_lateral > li > a');

        menu_ul.hide();

        menu_a.click(function(e) {
            e.preventDefault();
            if(!$(this).hasClass('active')) {
                menu_a.removeClass('active');
                menu_ul.filter(':visible').slideUp('normal');
                $(this).addClass('active').next().stop(true,true).slideDown('normal');
            } else {
                $(this).removeClass('active');
                $(this).next().stop(true,true).slideUp('normal');
            }
        });

    });


           /* $('.target').click (function(e) {
                var partido = $(".target");
                var idpartido=partido.data('idpartido');
                var equipo1=partido.data('equipo1');
                var equipo2=partido.data('equipo2');
                document.getElementById("cupon").innerHTML += "<li class='subitem3'><a href='#'>"+equipo1+"-"+equipo2+" <span>2</span></a></li>";  // Agrego nueva linea antes

        });
*/
    /*var ElementosClick = new Array();
    // Capturamos el click y lo pasamos a una funcion
    document.onclick = captura_click;

    function captura_click(e) {
        // Funcion para capturar el click del raton
        var HaHechoClick;
        if (e == null) {
            // Si hac click un elemento, lo leemos
            HaHechoClick = event.srcElement;
        } else {
            // Si ha hecho click sobre un destino, lo leemos
            HaHechoClick = e.target;
        }
        // Añadimos el elemento al array de elementos
        ElementosClick.push(HaHechoClick);
        // Una prueba con salida en consola
        console.log("Contenido sobre lo que ha hecho click: "+HaHechoClick);
    }*/
//añadir al cupon
    $('#equipos2222').click (function(e) {
        var id = e.target.parentNode.id;

        var partido = $("#"+id);
        //alert(partido.data('idpartido'))
        var idpartido=partido.data('idpartido');
        var equipo1=partido.data('equipo1');
        var equipo2=partido.data('equipo2');
        var cuota=partido.data('cuota');
        document.getElementById("cupon").innerHTML += "<li id=cupon"+idpartido+" data-idpartido='2' class='subitem3'><a href='#'>"+equipo1+"-"+equipo2+"<div style='float: left'>"+cuota+"</div> <span id='cerrarcupon'>X</span></a></li>";  // Agrego nueva linea antes
        $("#numero_elementos").html($("#cupon li").size());

    });

    //elimina una apuesta del cupon de apuestas
    $('#cupon').click (function(e) {
        var id = e.target.id;
        var idpartidow=e.target.parentNode.parentNode.id;
       // alert("id del li: "+idpartidow)
        var partido =$("#"+idpartidow);
        var idpartido=partido.data('idapuesta')
        //alert("id del partido a eliminar: "+idpartido)
        if (id=='cerrarcupon'){
            //alert('eliminando')
            var url = "/eliminarcombinada/"+idpartido;
            $(location).attr('href',url);
               // $("#"+idpartidow).remove();
        }
        /*//var partido = $("#"+id);
        $("#numero_elementos").html($("#cupon li").size());
        var numero=$("#cupon li").size()
        alert("numero partidos en la combinada: "+numero)

        alert($("#btnapostar").value)
        if (numero >0){
            alert('mayor que uno')

            if ($("#btnapostar")){
            alert('hola')
            $('#apostar').append("<li id='btnapostar' value='1'><a href='#'>Nuevo elemento</a></li>");
        }}*/

        //alert(partido.data('idpartido'))

    });
    $(document).ready(function(){
        /*$("#numero_elementos").html($("#cupon li").size());
        var numero=$("#cupon li").size()
        alert("numero partidos en la combinada: "+numero)

        alert($("#btnapostar").value)
        if (numero >0){
            alert('mayor que uno')

            if ($("#btnapostar")){
                alert('hola')
                $('#apostar').append("<li id='btnapostar' value='1'><a href='#'>Nuevo elemento</a></li>");
            }}*/
    });


    //apostar o combinar
    function seleccion( selection ){


        var form = document.getElementById('formApuesta');
        var rol = "";

        //Apostar
        if(selection == 1){
            form.action = "/apostar";

        }else{
            //combinar
            form.action = "/combinar";

        }

        $('#btn-soy-cuidadora').hide();
        document.getElementById("btn-soy-familia").disabled = true;

        var textoWait = "Espere unos segundos";
        $('#btn-soy-familia').html(textoWait);

        var i = 0;
        setInterval(function(){
            if(i==3){ i=0; textoWait = "Espere unos segundos"; }
            textoWait = textoWait+'.';
            $('#btn-soy-familia').html(textoWait);
            i++;
        }, 1000);

        form.submit();

    }
//añadir al modal
    $('#equipos').click (function(e) {
        var id = e.target.parentNode.id;
        var id_pronostico = e.target.id

        var partido = $("#"+id);
        //alert(partido.data('idpartido'))
        var idpartido=partido.data('idpartido');
        var equipo1=partido.data('equipo1');
        var equipo2=partido.data('equipo2');
        if(id_pronostico == 1){
            var cuota=partido.data('cuota1');
        }else {
            if(id_pronostico == "x"){
                var cuota=partido.data('cuota3');
            }else{
                var cuota=partido.data('cuota2');
            }
        }


        $('#partido_id').val(idpartido);
        $('#cuota_partido').val(cuota);

        $('#pronostico').val(id_pronostico);
        //alert('aaaaa')

    });

    $('#btnapostar').click (function(e) {

        var url = "/apostar-combi";
        $(location).attr('href',url);


    });



</script>

</body>
</html>