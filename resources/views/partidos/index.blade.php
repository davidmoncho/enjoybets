@extends('layouts.app_login')

@section('content')
    {{--<h1>Partidos del dia</h1>
    @foreach($partidos as $partido)
        <h2>{{$partido->id}}</h2>
    @endforeach--}}

            <!-- Page Title
		============================================= -->
        <section id="page-title" style="background-color: #FFA200">

            <div class="container clearfix">
                <h1>PARTIDOS DE LA JORNADA</h1>

            </div>

        </section><!-- #page-title end -->

        <!-- Content
        ============================================= -->
        <section id="content">

            <div class="content-wrap">

                <div class="clearfix">
                    <!-- Sidebar
                    ============================================= -->
                    <div class="col_one_fifth  nobottommargin clearfix">
                        <div class="sidebar-widgets-wrap">

                            <div id="menu_lateral">

                                <ul class="menu_lateral">
                                    <li class="item1"><a >Futbol <span>3</span></a>
                                        <ul>
                                            <li class="subitem1"><a href="/futbol/1">Liga española</a></li>
                                            <li class="subitem2"><a href="/futbol/3">Premier lague </a></li>
                                            <li class="subitem3"><a href="/futbol/2">Champions league</a></li>

                                        </ul>
                                    </li>
                                    <li class="item2"><a >Baloncesto <span>2</span></a>
                                        <ul>
                                            <li class="subitem1"><a href="/baloncesto/1">ACB <span>14</span></a></li>
                                            <li class="subitem2"><a href="/baloncesto/2">NBA<span>6</span></a></li>

                                        </ul>
                                    </li>

                                </ul>

                            </div>




                        </div>
                    </div><!-- .sidebar end -->

                    <!-- Post Content
                    ============================================= -->
                    <div class="col_three_fifth nobottommargin clearfix" >
                        <div id="equipos"><?php $partidos ?>
                            <div >
                                <h2>Futbol</h2>
                                @foreach($partidos as $partido)
                                    @if($partido->competicion->deporte->id==1)
                                        <div id="partido_{{$partido->id}}" data-idpartido="{{$partido->id}}" data-cuota1="{{$partido->cuota_eq1}}"data-cuota2="{{$partido->cuota_eq2}}" data-cuota3="{{$partido->cuota_empate}}"  data-equipo1="{{$partido->equipo1->nombre}}" data-equipo2="{{$partido->equipo2->nombre}}" >

                                            <div id="1" class="partidos black partidos_large topmargin-sm" data-toggle="modal" data-target=".bs-example-modal-lg" >{{$partido->equipo1->nombre}} <span>{{$partido['cuota_eq1']}}</span></div>
                                            <div id="x" class="partidos black partidos_medium topmargin-sm" data-toggle="modal" data-target=".bs-example-modal-lg">X<span>{{$partido->cuota_empate}}</span></div>
                                            <div id="2" class="partidos black partidos_large topmargin-sm" data-toggle="modal" data-target=".bs-example-modal-lg">{{$partido->equipo2->nombre}}<span>{{$partido['cuota_eq2']}}</span></div>
                                        </div>

                                    @endif
                                @endforeach
                            </div>
                            <div>
                                <h2>Baloncesto</h2>
                                @foreach($partidos as $partido)
                                    @if($partido->competicion->deporte->id == 2)
                                        <div id="{{$partido->id}}" data-idpartido="{{$partido->id}}" data-equipo1="{{$partido->equipo1->nombre}}" data-equipo2="{{$partido->equipo2->nombre}}">

                                            <div id="target" class="partidos black partidos_large2 topmargin-sm" data-toggle="modal" data-target=".bs-example-modal-lg">{{$partido->equipo1->nombre}} <span>{{$partido['cuota_eq1']}}</span></div>
                                            <div class="partidos black partidos_large2 topmargin-sm" data-toggle="modal" data-target=".bs-example-modal-lg">{{$partido->equipo2->nombre}}<span>{{$partido['cuota_eq2']}}</span></div>
                                        </div>
                                    @endif
                                @endforeach
                            </div>
                        </div>

                    </div><!-- .postcontent end -->

                        <!-- Sidebar
                                  ============================================= -->
                        <div class="col_one_fifth nobottommargin col_last clearfix">
                            <div class="sidebar-widgets-wrap">

                                <div id="menu_lateral">

                                    <ul class="menu_lateral">
                                        <li class="item1"><a href="#">Mis apuestas<span >{{$num_apuestas}}</span></a>
                                            <ul>
                                                @foreach($apuestas as $apuesta)
                                                    <li class="subitem1" style="border-bottom: solid #FFA200 1px"><a href="#"  style="height: auto;border:#383838 1px solid">
                                                    @foreach($apuesta->partidos as $partido)

                                                           <div >{{$partido->equipo1->nombre_corto}}-{{$partido->equipo2->nombre_corto}}</div>


                                                    @endforeach
                                                        <div style="width:48%;display: inline-block;clear: both">
                                                            <div style="">Importe</div>
                                                            <div style="">{{$apuesta->cantidad_apostada}}</div>
                                                        </div>
                                                        <div style="width: 48%;display: inline-block; clear: both">
                                                            <div style="width: 100%">Cuota</div>
                                                            <div style="width: 100%">{{$apuesta->cuota}}</div>
                                                        </div>
                                                        </a></li>
                                                @endforeach
                                            </ul>
                                        </li>
                                        <li class="item1" ><a href="#">Cupon<span id="numero_elementos">{{$num_preapuestas}}</span></a>

                                            <ul id="cupon">

                                                <ul>
                                                    @foreach($cupon as $apuesta)


                                                        <li id="cupon-{{$apuesta->id}}" data-idapuesta="{{$apuesta->id}}"class="subitem1" style="border-bottom: solid #FFA200 1px"><a href="#"  style="height: auto;border:#383838 1px solid">
                                                                @foreach($apuesta as $partido)


                                                                    <div></div>


                                                                @endforeach

                                                                <div style="width:48%;display: inline-block;clear: both">
                                                                    <div style="">Importe</div>
                                                                    <div style=""></div>
                                                                </div>
                                                                <div style="width: 48%;display: inline-block; clear: both">
                                                                    <div style="width: 100%">Cuota</div>
                                                                    <div style="width: 100%"></div>
                                                                </div>


                                                                    <span id='cerrarcupon'>x</span></a></li>

                                                    @endforeach
                                                        @if($num_preapuestas>1)
                                                            <li id='btnapostar' value='1'><a href='#'>Apostar</a></li>
                                                        @endif

                                            </ul>


                                        </li>

                                    </ul>

                                </div>





                            </div>
                        </div><!-- .sidebar end -->


                </div>

            </div>

        </section><!-- #content end -->



<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-body">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel">Apuesta</h4>
                </div>
                <div class="modal-body">
                    <form id="formApuesta"style="color: red" role="form" action="" method="POST">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <label>Id partido</label>
                        <input type="" name="partido_id" id="partido_id"value="">
                        <label>Cuota</label>
                        <input type="" name="cuota_partido"id="cuota_partido" value="">
                        <label>Cantidad apostada</label>
                        <input type="" name="cantidad_apostada"id="cantidad_apostada" value="">
                        <label>Pronostico</label>
                        <input type="" name="pronostico"id="pronostico" value="">


                        <div class="col_full nobottommargin center">
                            <button id="btn-soy-cuidadora" class="button button-rounded  button-large button-border tright" onclick="seleccion(1)">Apostar</button>
                        </div>
                        <div class="col_full nobottommargin center">
                            <button id="btn-soy-familia" class="button button-rounded button-large button-border tright" onclick="seleccion(2)">Combinar </button>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>



@endsection
