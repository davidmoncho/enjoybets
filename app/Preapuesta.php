<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Preapuesta extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'preapuesta';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
        'id', 'id_user', 'pronostico','id_partido','cupon_id'
    ];
    public function user(){
        return $this->belongsTo('App\Users');
    }

    public function partidos(){
        return $this->belongsToMany('App\Partido','apuesta_partido','id_apuesta','id_partido');
    }
    public function cupones()
    {
        return $this->belongsToMany('App\Cupon');


    }

    public $timestamps = false;
}
