<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Equipo extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'equipo';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
        'nombre', 'nombre_corto'
    ];
    public function equipo_1(){
        return $this->hasMany('App\Partido','equipo_1');
    }
    public function equipo_2(){
        return $this->hasMany('App\Partido','equipo_2');
    }

    public function competiciones()
    {
        return $this->belongsToMany('App\Competicion','competicion_equipo','id_equipo','id_competicion');
    }

    public $timestamps = false;
}
