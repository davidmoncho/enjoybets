<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 21/05/16
 * Time: 1:20
 */

namespace App\Services;


abstract class FactoryGames
{
    protected $createdGame;
    abstract protected function createGame($tipoPartido);

   public function create($tipoPartido)
    {
        $obj = $this->createGame($tipoPartido);

        return $obj;
    }
}