<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Deporte extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'deporte';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
        'nombre'
    ];
    public function competiciones(){
        return $this->hasMany('App\Competicion','id_deporte','id');
    }
    public $timestamps = false;
}
