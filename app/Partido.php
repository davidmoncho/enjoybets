<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Partido extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'partido';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
        'id_competicion', 'equipo_1', 'equipo_2','cuota_empate'
    ];
    public function equipo1()
    {
        return $this->belongsTo('App\Equipo','equipo_1');

    }
    public function equipo2()
    {
        return $this->belongsTo('App\Equipo','equipo_2');


    }
    public function competicion(){
        return $this->belongsTo('App\Competicion','id');
    }

    public function apuestas(){
        return $this->belongsToMany('App\Partido','apuesta_partido','id_partido','id_apuesta');
    }


    public $timestamps = false;
}
