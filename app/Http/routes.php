<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/*Route::get('/', function () {
    return view('welcome');
});

Route::auth();

Route::get('/home', 'HomeController@index');*/

Route::group(['Midelware'=>['web']],function(){
    Route::get('/', function () {
        return view('welcome');
    });

    Route::auth();
    Route::get('/home', 'HomeController@index');

    Route::get('p', function(){

        $apuesta = \App\Apuesta::find(12);
        dd($apuesta);

        $apuesta->partidos()->attach(1);


    });

    Route::get('/partidos', 'PartidosController@index');

    Route::get('/futbol', 'PartidosController@partidosfutbol');
    Route::get('/futbol/{competicion}', 'PartidosController@competicionesfutbol');

    Route::get('/baloncesto', 'PartidosController@partidosbaloncesto');
    Route::get('/baloncesto/{competicion}', 'PartidosController@competicionesbaloncesto');

    Route::get('/tenis', 'PartidosController@tenis');
    Route::get('/tenis/{competicion}', 'PartidosController@competicionestenis');

    Route::post('/apostar','ApuestasController@apostar');
    Route::post('/combinar','ApuestasController@combinar');

    Route::get('/eliminarcombinada/{id_partido}','ApuestasController@descombinar');
    Route::get('/apostar-combi','ApuestasController@apostarcombi');

});


