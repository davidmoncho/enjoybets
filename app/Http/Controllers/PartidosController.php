<?php

namespace App\Http\Controllers;

use App\Apuesta;
use App\Cupon;
use App\Deporte;
use App\Equipo;
use App\Partido;
use App\Services\FactoryFootball;


use App\Http\Requests;
use Illuminate\Http\Request;

class PartidosController extends Controller
{
    public function index(Request $request)
    {

        $user=$request->user();
        $partidos=Partido::all();
        $cupon=Cupon::where('id_usuario',$user['id'])->get();
        $apuestas=Apuesta::where('id_user',$user['id'])->get();
        $num_apuestas=Apuesta::where('id_user',$user['id'])->count();
        $num_preapuestas=Cupon::where('id_usuario',$user['id'])->count();
        //foreach($cupon as $partido)
       //dd($partido->apuestas);
        //dd($apuestas[0]->partidos );

        if (!isset($user)) {
            return view('partidos.index',['partidos' => $partidos]);
        }

        return view('partidos.index',['partidos' => $partidos,'cupon'=>$cupon,'apuestas'=>$apuestas,'num_apuestas'=>$num_apuestas,'num_preapuestas'=>$num_preapuestas]);
    }

    public function futbol()
    {
        $deporte=Deporte::find(2);
        //$partidos= new FactoryFootball();
        // dd($deporte->id);

        return view('partidos.index',['partidos' => $deporte]);
    }
    public function baloncesto()
    {
        $deporte=Deporte::find(1);
        //$partidos= new FactoryBasketball;
       // dd($deporte->id);

        return view('partidos.index',['partidos' => $deporte]);
    }
}
