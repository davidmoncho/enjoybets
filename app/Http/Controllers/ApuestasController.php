<?php

namespace App\Http\Controllers;

use App\Apuesta;
use App\Cupon;
use App\User;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Input;
class ApuestasController extends Controller
{
    public function index()
    {

        return view('apuestas.index');
    }

    public function apostar(Request $request){//bet
        $user = $request->user();
        //dd($user->id);
        if (!isset($user)) {
            return view('errors/503');
        }

        $data =$request->all();

        $cuota=$data['cuota_partido'];
        $combinadas=Cupon::where('id_usuario',$user->id)->get();
        foreach($combinadas as $apuesta){
            $cuota=$cuota*$apuesta->cuota;
            $apuesta->delete();
        }

        $apuesta= new Apuesta();
        $apuesta->id_user=$user->id;
        $apuesta->id_partido=$data['partido_id'];
        $apuesta->cantidad_apostada=$data['cantidad_apostada'];
        $apuesta->cuota=$cuota;
        $apuesta->pronostico=$data['pronostico'];
        $apuesta->save();
        //dd($apuesta);
        $apuesta->partidos()->attach($data['partido_id']);

        return back();
    }

    public function combinar(Request $request){//add
        $user = $request->user();
        //dd($user->id);
        if (!isset($user)) {
            return view('errors/503');
        }

        $data =$request->all();


        $combinada=Cupon::where('id_usuario',$user->id)->where('id_partido',$data['partido_id'])->get();

        //dd($combinada[0]);
        if(!empty($combinada[0])){
            dd('este partido no se puede combinar');
            return back()->with(['combinada'=>$combinada]);
        }else{
            $combinada=new Cupon();
            $combinada->id_partido=$data['partido_id'];
            $combinada->id_usuario=$user->id;
            $combinada->cuota=$data['cuota_partido'];

            $combinada->save();
            $combinadas=Cupon::where('id_usuario',$user->id)->get();

            return back()->with(['combinada'=>$combinadas]);
        }

        
    }

    public function descombinar(Request $request,$id_apuesta){//remove
        $user = $request->user();
        //dd($user->id);
        if (!isset($user)) {
            return view('errors/503');
        }

        $combinadas=Cupon::find($id_apuesta);
        $combinadas->delete();

        return back()->with(['combinada'=>$combinadas]);
    }

    public function apostarcombi(Request $request){

        $user = $request->user();
        if (!isset($user)) {
            return view('errors/503');
        }

        $combinada=Cupon::where('id_usuario',$user->id)->get();
       // dd($combinada);

        $id_usuario=$user->id;
        $cantidad="20";
        $cuota=1.0;
        $pronostico="1";

        $id_partido="";


        $apuesta = new Apuesta();

        $apuesta->id_user=$id_usuario;
        $apuesta->cantidad_apostada=$cantidad;

        $apuesta->pronostico=$pronostico;
       // $apuesta->id_partido=$id_usuario;
        $apuesta->save();

        foreach($combinada as $partido){

            $apuesta->partidos()->attach($partido->id_partido);
//dd($partido->cuota);
            $cuota=$cuota*$partido->cuota;
         //   dd($cuota);
            $partido->delete();

        }

        $apuesta->cuota=$cuota;
        $apuesta->save();
        //dd($apuesta);
        return back();
        //dd($combinada);
    }
}
