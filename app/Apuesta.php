<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Apuesta extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'apuesta';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
        'id', 'cantidad_apostada', 'pronostico',
    ];
    public function user(){
        return $this->belongsTo('App\Users');
    }

    public function partidos(){
        return $this->belongsToMany('App\Partido','apuesta_partido','id_apuesta','id_partido');
    }

    public $timestamps = false;
}
