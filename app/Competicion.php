<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Competicion extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'competicion';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
        'id','nombre','id_deporte'
    ];
    public function deporte(){
        return $this->belongsTo('App\Deporte','id_deporte');
    }
    public function partidos(){
        return $this->hasMany('App\Deporte','id_competicion');
    }

    public function equipos()
    {
        return $this->belongsToMany('App\Equipo','competicion_equipo','id_equipo','id_competicion');
    }


    public $timestamps = false;
}
