<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cupon extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'cupon';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
        'id', 'cuota', 'id_partido','id_usuario'
    ];
    public function user(){
        return $this->hasOne('App\Users');
    }
    public function apuestas(){
        return $this->hasMany('App\Preapuesta','cupon_id');
    }

    public $timestamps = false;


}
